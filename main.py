import argparse
import sys
from preprocess import Preprocessor
import logging

def get_args():
    parser = argparse.ArgumentParser('kin-timepix-visualization')
    parser.add_argument('-d', '--data', default='KIN_data/SATRAM_PROBA-V_data_from_2018-03-01.txt',
                        help='path to the radiation data matrix file (.txt)')
    parser.add_argument('-i', '--header', default='KIN_data/SATRAM_PROBA-V_data_from_2018-03-01.txt.dsc',
                        help='path to the information header file (.dsc)')
    parsed_args = parser.parse_args()
    return parsed_args


def main():
    try:
        arg = get_args()
        if arg.data.split('.')[-1].lower() != 'txt':
            logging.info('Wrong DATA file format', arg.data)
            return 1
        if arg.header.split('.')[-1].lower() != 'dsc':
            logging.info('Wrong HEADER file format', arg.header)
            return 2

        logging.basicConfig(level = logging.INFO)

        preproc = Preprocessor()

        logging.info("Data loading started.")
        # preproc.load_data('KIN_data\SATRAM_PROBA-V_data_from_2018-03-01.txt','KIN_data\SATRAM_PROBA-V_data_from_2018-03-01.txt.dsc')
        preproc.load_data(arg.data, arg.header)
        logging.info("Finished loading data, ready to process")
        preproc.preprocess()
        preproc.calculate_rotations()
        logging.info("Preprocessing finished")
        preproc.save_data()
    except Exception as e:
        logging.info(e)
        return 3
    logging.info("Done")
    return 0


if __name__ == "__main__":
    sys.exit(main())
