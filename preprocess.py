from frame import Frame
import numpy as np
import logging

import xlsxwriter as xls


class Preprocessor():

    """ Class that stores the data and has processing methods """

    def __init__(self):
        self.data = []
        logging.basicConfig(level = logging.INFO)

    def preprocess(self):

        """ Method that takes all the frames in self.data, groups them into clusters and writes into file. """

        if len(self.data) == 0:
            logging.info("Empty data, load them first!")
            return

        # extract the name out of the first frame    

        timestamp = self.data[0].data['Timestamp']
        date,time = timestamp.split()

        file_name = "radiation-data-" + date + ".clog"

        f = open(file_name,"w")

        # BFS clustering

        for frame in self.data:
            # write the header
            f.write("Frame "+str(frame.id)+" (" + str(frame.data["Start time"]) + ", " + str(frame.data["Acq time"]) + " s)\n")
            visited = np.zeros((frame.height,frame.width))
            values = []
            subvalues = []
            for y in range(frame.height):
                for x in range(frame.width):
                    if visited[y,x] == 1:
                        # skip if already explored
                        continue
                    if frame.matrix[y,x] == 0:
                        # if empty add to visited and skip
                        visited[y,x] = 1
                        continue

                    # these are not visited pixels with data

                    val = [x,y,int(frame.matrix[y,x])]
                    subvalues.append(val)
                    visited[y,x] = 1

                    # candidates for clustering in 4 directions
                    candidates = [[y+1,x],[y-1,x],[y,x+1],[y,x-1]]

                    while candidates:
                        candidate = candidates.pop(0)
                        yt = candidate[0]
                        xt = candidate[1]

                        if yt<0 or xt<0 or yt>=frame.width or xt>=frame.height:
                            # skip if out of frame
                            continue

                        if visited[yt,xt] == 1:
                            # skip if already explored
                            continue

                        visited[yt,xt] = 1

                        if frame.matrix[yt,xt] == 0:
                            continue

                        val = [xt,yt,int(frame.matrix[yt,xt])]

                        subvalues.append(val)

                        # add new candidates
                        candidates.append([yt+1,xt])
                        candidates.append([yt-1,xt])
                        candidates.append([yt,xt+1])
                        candidates.append([yt,xt-1])
                    
                    values.append(subvalues)
                    subvalues = []

            # write into file
            for s in values:
                for i,p in enumerate(s):
                    if i < len(s):
                        f.write(str(p) + " ")
                    else:
                        f.write(str(p))

                f.write("\n")                       

            f.write("\n")
            
            # log the finished frame to terminal
            logging.info("Finished frame %d/%d",frame.id,len(self.data))

        # close the file
        f.close()

    def load_data(self,measurement_path,info_path):

        """
        Method that loads the inputs and fills the self.data.
        measurement_path    -->         path of the radiaton matrix file
        info_path           -->         path of the information file
        """

        separator = "#"                                 # separator of measurement_file between frames
        info_seperator = '\n\n\n'                       # separator of information_file between frames

        # open the measurement_file and split the individual frames
        f = open(measurement_path,"r")
        measurements = f.read().split(separator)
        f.close()

        # open the information_file and split the individual frames
        f = open(info_path,"r")  
        info = f.read().split(info_seperator)
        f.close()

        # zip inputs to be looped over
        items = zip(measurements,info)

        # loop over all the frames and populate self.data
        for index, item in enumerate(items,start=0):
            frame = Frame(item[0],item[1],index)
            self.data.append(frame)

    def calculate_rotations(self):

        """ Method calculate Euler angles and rotation matrix from given quaternion for each frame of data """

        eps = 1e-10

        # check if some data are available
        if len(self.data) == 0:
            logging.info("Empty data, load them first!")
            return

        # loop through the frames
        for frame in self.data:
            # load quaternion - we assume them in the w, x, y, z order
            q = frame.data['Satellite attitude']     # w, x, y, z
            [w, x, y, z] = q

            # check that they are normalised
            n = np.linalg.norm(q)
            if abs(n-1) > eps:
                print("Should be normalized")

            # calculate and save the Euler angles from quaternion
            yaw = np.arctan2(2*(w*x + y*z), 1-2*(x**2 + y**2))
            pitch = np.arcsin(2*(w*y - z*x))
            roll = np.arctan2(2*(w*z + x*y), 1-2*(y**2 + z**2))
            frame.data['Euler angles'] = [yaw, pitch, roll]

            # calculate and save the rotation matrix from quaternion (ECI to BoF)
            rotation_matrix = np.array([
                [w ** 2 + x ** 2 - y ** 2 - z ** 2, 2 * (x * y - z * w), 2 * (x * z + y * w)],
                [2 * (x * y + z * w), w ** 2 - x ** 2 + y ** 2 - z ** 2, 2 * (y * z - x * w)],
                [2 * (x * z - y * w), 2 * (y * z + x * w), w ** 2 - x ** 2 - y ** 2 + z ** 2]
            ])
            frame.data['Rotation from ECI to BoF'] = rotation_matrix
            # inverse of rotation matrix is rotation matrix from BoF to ECI
            # inverse = rotation_matrix.T

    def save_data(self):

        """ Method save the information data in xlsx file, where each line is a different frame. """

        if len(self.data) == 0:
            logging.info("Empty data, load them first!")
            return

        # extract the name out of the first frame    

        timestamp = self.data[0].data['Timestamp']
        date,time = timestamp.split()

        file_name = "navig-" + date + ".xlsx"

        # create new excel file and worksheet
        book = xls.Workbook(file_name)
        sheet = book.add_worksheet()

        # create the first 3 lines (header)
        header_names = ['Frame','ACQ Time','Year','Month','Day','Hours','Minutes','Seconds','Miliseconds','Unix Time','Latitude','Longitude','Altitude','Quaternion W', 'Quaternion X','Quaternion Y','Quaternion Z','Temperature Min','Temperature Max','Bias Min','Bias Max','Mag fie 1','Mag fie 2','Mag fie 3','Timepix clock','DAC THL','DAC THL', 'Yaw','Pitch','Roll']
        header_description = ['[#]','[s]','[#]','[#]','[#]','[#]','[#]','[#]','[#]','[#]','[deg]','[deg]','[m]','[#]','[#]','[#]','[#]','[deg]','[deg]','[V]','[V]','[B]','[B]','[B]','[MHz]','[coarse]','[fine]','[rad]','[rad]','[rad]']

        header = zip(header_names,header_description)

        underline = book.add_format({'bottom': True})

        for index,value in enumerate(header):
            sheet.write(0,index,value[0])
            sheet.write(1,index,value[1])
            sheet.write(2,index,index+1,underline)

        # now fill the rest of the data

        row = 3

        # loop through the frames
        for frame in self.data:
            sheet.write(row,0,frame.id)                     # FRAME ID
            sheet.write(row,1,frame.data['Acq time'])       # ACQ time

            # TIME STAMP
            timestamp = frame.data['Timestamp']
            date,time = timestamp.split()
            year,month,day = date.split('-')
            hours,minutes,seconds = time.split(':')
            s,ms = seconds.split('.')

            sheet.write(row,2,int(year))                         # Year
            sheet.write(row,3,int(month))                        # Month
            sheet.write(row,4,int(day))                          # Day
            sheet.write(row,5,int(hours))                        # Hour
            sheet.write(row,6,int(minutes))                      # Minutes
            sheet.write(row,7,int(s))                            # Seconds
            sheet.write(row,8,int(ms))                           # Miliseconds

            sheet.write(row,9,frame.data['Start time'])     # UNIX time

            gps = frame.data['Satellite position']

            sheet.write(row,10,gps[2])                      # Latitude
            sheet.write(row,11,gps[1])                      # Longitude
            sheet.write(row,12,gps[0])                      # Altitude

            quaternion = frame.data['Satellite attitude']

            sheet.write(row,13,quaternion[0])               # Quaternion W
            sheet.write(row,14,quaternion[1])               # Quaternion X
            sheet.write(row,15,quaternion[2])               # Quaternion Y
            sheet.write(row,16,quaternion[3])               # Quaternion Z

            temp = frame.data['Internal temperature']

            sheet.write(row,17,temp[1])                     # Temperature Min
            sheet.write(row,18,temp[0])                     # Temperature Max

            bias = frame.data['Bias']

            sheet.write(row,19,bias[1])                     # Bias Min
            sheet.write(row,20,bias[0])                     # Bias Max

            mag = frame.data['Magnetic field strength [B]']

            sheet.write(row,21,mag[0])                      # Mag 1
            sheet.write(row,22,mag[1])                      # Mag 2
            sheet.write(row,23,mag[2])                      # Mag 3

            sheet.write(row,24,frame.data['Timepix clock']) # Timepix clock

            dac = frame.data['DACs']

            sheet.write(row,25,dac[6])                      # DAC course
            sheet.write(row,26,dac[7])                      # DAC fine

            euler = frame.data['Euler angles']

            sheet.write(row, 27, euler[0])                  # yaw
            sheet.write(row, 28, euler[1])                  # pitch
            sheet.write(row, 29, euler[2])                  # roll

            row += 1

        book.close()

        
        
