# Timepix data visualisation

## KIN Semestral project 2022

## Authors:
- Jakub Hortenský, <hortejak@fel.cvut.cz>
- Michal Koldinský, <koldimi1@fel.cvut.cz>


## Teamleaders:
- Hab. prof. Ing. Carlos Granja, Ph.D., <carlos.granja@advacam.cz>
- Ing. Martin Urbam, <martin-urban@fel.cvut.cz>

## Python script usage

Run it using `python3 main.py` with parameters as follows:

```
usage: kin-timepix-visualization [-h] [-d DATA] [-i HEADER]

options:
  -h, --help            show this help message and exit
  -d DATA, --data DATA  path to the radiation data matrix file (.txt)
  -i HEADER, --header HEADER
                        path to the information header file (.dsc)
```

Output is saved to the root of the project (file `radiation-data-YYYY-MM-DD.clog` and `navig-YYYY-MM-DD.xlsx`).