import numpy as np
import logging

class Frame():

    """ Class that stores the data and the information of the individual frame. """

    def __init__(self,measurements,info,index):
        
        self.matrix = None
        self.data = dict()
        self.id = index

        self.type = None
        self.width = None
        self.height = None

        logging.basicConfig(level = logging.INFO)

        self._process_info(info)
        self._process_measurements(measurements)
        


    def _process_measurements(self,m):

        """
        Method that processes the radiation matrix data and fills the matrix.
        m           -->         list of measurements [string]
        """

        if len(m) < 3:
            return
        # splits the lines of individual pixels
        measurements = list(filter(None,m.splitlines()))
        # fill the pixel of each hit
        for measurement in measurements:
            vals = measurement.split()
            index = int(vals[0])
            val = int(vals[1])

            x = index % self.width
            y = index // self.width

            self.matrix[y,x] = val

    def _process_info(self,i):

        """
        Method that processes the information data.
        i           -->         list of information chunks [string]
        """

        # find the index
        header_symbol = "[F" + str(self.id) + "]"
        header_index = i.find(header_symbol)
        if header_index == -1:
            logging.info("Given index does not corespond to expected values!")
            return False

        # identify indivudal chunks of information, each containing one parameter
        chunks = i[header_index:].split('\n\n')

        # pop the header
        header = chunks.pop(0)
        if not self._process_header(header):
            logging.info("Incorrect header!")
            return False


        for chunk in chunks:
            
            if not self._process_chunk(chunk):
                print("Incorrect chunk!")
                return False

    def _process_header(self,header):
        """
        Method that processes the header and stores all the information in it
        header      -->         the header [string]
        """
        tp = header.partition("Type=")[2]
        self.type = tp.split()[0]
        width = header.partition("width=")[2]
        self.width = int(width.split()[0])
        height = header.partition("height=")[2]
        self.height = int(height.split()[0])

        if header.find("Timestamp"):
            timestamp = header.splitlines()[4]
            self.data.update({"Timestamp":timestamp})

        # create the data matrix when we finally know the dimensions
        self.matrix = np.zeros((self.width,self.height))
        return True

    def _process_chunk(self,chunk):

        """
        Method that processes the chunk and stores all the information in it
        chunk      -->         chunk containing the information of one parameter [string]
        """

        lines = chunk.splitlines()

        value = self._determine_type(lines[1],lines[2])
        name = lines[0].split('"')[1]

        self.data.update({name:value})

        return True

    def _determine_type(self,line,value):

        """
        Method that determines the type of a value based on the written type
        line            -->         line containing the type and the number of chars [string]
        value           -->         the actual value [string]
        """

        tp,count = line.replace(']','[').split('[')[0:2]

        count = int(count)

        value = value.split()
        
        # if the value is string, leave it
        if tp == "char":
            if len(value) == 1:
                return value[0]
            return value

        values = []

        for val in value:
            if tp == "byte" or tp == "u16":
                val = int(val)
            elif tp == "float" or tp == "double":
                val = float(val)
            
            values.append(val)

        if len(values) == 1:
            return values[0]

        return values